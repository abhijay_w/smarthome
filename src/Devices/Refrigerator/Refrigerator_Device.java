package Devices.Refrigerator;

public class Refrigerator_Device extends Devices.Device {
    int temp_cur;
    int power_cons;

    public void setTemp_cur(int temp_cur) {
        this.temp_cur = temp_cur;
    }

    public void setPower_cons(int power_cons) {
        this.power_cons = power_cons;
    }

    public int getTemp_cur() {
        return temp_cur;
    }

    public int getPower_cons() {
        return power_cons;
    }
}
