package Devices.Washing_Machine;

public class Washing_Machine_Device extends Devices.Device {
    String program;
    int stage;
    int water_temp;

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public int getStage() {
        return stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
    }

    public int getWater_temp() {
        return water_temp;
    }

    public void setWater_temp(int water_temp) {
        this.water_temp = water_temp;
    }
}
