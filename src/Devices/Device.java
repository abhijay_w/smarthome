package Devices;

public class Device {
    String device_id;
    boolean status;
    int type;

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public boolean isStatus() {
        return status;
    }

    public int getType() {
        return type;
    }
}

